package com.allsmart.math;

/**
 * Class that calculates the sum of numbers
 */
public class Add {

    /**
     * This function returns the sum of two numbers
     *
     * @param a int First Number
     * @param b int Second Number
     * @return int Sum of both the Numbers
     */
    public static int numbers(int a, int b) {
        return a + b;
    }

    /**
     * This function returns the sum of three numbers
     *
     * @param a int First Number
     * @param b int Second Number
     * @param c int Third Number
     * @return int Sum of three numbers
     */
    public static int numbers(int a, int b, int c) {
        return a + b + c;
    }

    /**
     * This function returns the sum of an array of integers
     *
     * @param a int[] integer array
     * @return int sum of the integers in array
     */
    public static int numbers(int[] a) {
        int ans = 0;
        for (int i = 0; i <= a.length; i++) {
            ans = ans + a[i];
        }
        return ans;
    }
}
