package com.allsmart.math;

/**
 * Class that calculates the div of numbers
 */
public class Div {
    /**
     * This function returns the div of two numbers
     *
     * @param a int First Number
     * @param b int Second Number
     * @return int div of both the Numbers
     */
    public static int numbers(int a, int b) {
        if (b != 0) return a / b;
        else return 0;
    }

    /**
     * This function returns the div of three numbers
     *
     * @param a int First Number
     * @param b int Second Number
     * @param c int Third Number
     * @return int div of three numbers
     */
    public static int numbers(int a, int b, int c) {
        if (b != 0 && c != 0)
            return a / b / c;
        else return 0;
    }

    /**
     * This function returns the div of an array of integers
     *
     * @param a int[] integer array
     * @return int div of the integers in array
     */
    public static int numbers(int[] a) {
        int ans = 0;
        for (int i = 0; i <= a.length; i++) {
            if (a[i] != 0)
                ans = ans / a[i];
            else return 0;
        }
        return ans;
    }
}