package com.allsmart.math;

/**
 * Class that calculates the mul of numbers
 */
public class Mul {

    /**
     * This function returns the mul of two numbers
     *
     * @param a int First Number
     * @param b int Second Number
     * @return int mul of both the Numbers
     */
    public static int numbers(int a, int b) {
        return a * b;
    }

    /**
     * This function returns the mul of three numbers
     *
     * @param a int First Number
     * @param b int Second Number
     * @param c int Third Number
     * @return int mul of three numbers
     */
    public static int numbers(int a, int b, int c) {
        return a * b * c;
    }

    /**
     * This function returns the mul of an array of integers
     *
     * @param a int[] integer array
     * @return int mul of the integers in array
     */
    public static int numbers(int[] a) {
        int ans = 0;
        for (int i = 0; i <= a.length; i++) {
            ans = ans * a[i];
        }
        return ans;
    }
}
