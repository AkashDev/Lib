package com.allsmart.libarytesting;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.allsmart.math.Add;
import com.allsmart.math.Div;
import com.allsmart.math.Mul;
import com.allsmart.math.Power;
import com.allsmart.math.Sub;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i("Sum", String.valueOf(Add.numbers(4, 5)));
        Log.i("Sum", String.valueOf(Add.numbers(4, 5, 2)));
        Log.i("Sum", String.valueOf(Add.numbers(new int[]{1, 4, 6, 7})));
        Log.i("Sub", String.valueOf(Sub.numbers(4, 5)));
        Log.i("Mul", String.valueOf(Mul.numbers(4, 5)));
        Log.i("Div", String.valueOf(Div.numbers(4, 5)));
        Log.i("Power", String.valueOf(Power.numbers(3, 5)));
    }
}
